<?php

    //session_start();
 
try
{
    $pdo = new PDO("mysql:host=localhost;dbname=klomputry", "root", "");
    // Set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 
    catch(PDOException $e)
{
    die("ERROR: Could not connect. " . $e->getMessage());
}
 
// Attempt search query execution
try{
    if(isset($_REQUEST["term"])){
        // create prepared statement
        $sql = "SELECT * FROM myproduct WHERE title LIKE :term";
        $stmt = $pdo->prepare($sql);
        $term = $_REQUEST["term"] . '%';
        // bind parameters to statement
        $stmt->bindParam(":term", $term);
        // execute the prepared statement
        $stmt->execute();
        if($stmt->rowCount() > 0){
            while($row = $stmt->fetch())
            {
//                echo "<img width = \"50\" height = \"50\" src = \"".$row["image"]."\">";
//                echo "<h5>" . $row["title"] . " | " . $row["id_myproduct"] . " | " . $row["description"] . " | " . $row["price"]. "zł </h5>";
//                
                echo "
                <h5>
                <table class = \"sturdy\">
                    <tr>
                        <th></th>
                        <th></th> 
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td><img style = \"max-width: 72px;\" src = \"".$row["image"]."\"></td>
                        <td>" . $row["id_myproduct"] . "</td>
                        <td>" . $row["title"] . "</td>
                        <td>" . $row["description"] . "</td>
                        <td>" . $row["price"] . "zł</td>
                        <td>" . $row["category"] . "</td>
                      </tr>
                </table>
                </h5>
                ";
            }
        } else{
            echo "<p>Nie znaleziono produktu</p>";
        }
    }  
} catch(PDOException $e){
    die("ERROR: Could not able to execute $sql. " . $e->getMessage());
}
 
// Close statement
unset($stmt);
 
// Close connection
unset($pdo);
?>